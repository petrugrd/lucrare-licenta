/*
 * remote.cpp
 *
 *  Created on: 3 mai 2018
 *      Author: Petru Gurduza
 */

#include "remote.h"

void (*resetFunc)(void) = 0;

/*
 * begin variable defining
 */

decodeRemoteInstructionsStatus_type decodeInstructionsStatus;
uint16_t write_InstructionBuffer_index = 0, read_ReceivedString_index = 0;
//bool remoteNewData = false, remoteNewInstruction = false;
remoteInstructionRunStatus_type remoteInstructionRunStatus;

/*
 * end variable defining
 */

remoteReceivedDataStatus_type remote_ReceiveInstructions(uint8_t *receivedBuffer)
{
	static uint16_t receivedCharBuffer_index = 0;
	if (REMOTE.available() > 0)
	{
		while (REMOTE.available() > 0)
		{
			receivedBuffer[receivedCharBuffer_index] = REMOTE.read();
			receivedCharBuffer_index++;
			delay(2);
		}
		Serial.print("Sir citit\n");
		if (REMOTE.available() == 0)
		{
			if (REMOTE.available() == 0)
				return REMOTE_RECEIVED_TRUE;

			else
				return REMOTE_IN_PROCESS;
		}
	}
	return REMOTE_RECEIVED_FALSE;

}

//
// this should be static
void remote_StoreInstruction(uint8_t * buffer,
		instructionData_type *outputInstructionsBuffer)
{
	uint8_t parameterValuePower = 0;
	uint16_t parameterValue = 0;
	remoteInstructionReceived_type receivedCommand;

//	if (remoteNewInstruction == true)
	{
		// do all the work here

		/*
		 * Instructions buffer template:
		 * buffer[0] - instruction type
		 * buffer[1] - delimiter ' _ '
		 * buffer[2..4] - parameter value
		 * buffer[5] - instruction end
		 */

		/*
		 * 1. citesc instructiunea, daca exista separatorul
		 * 			setez un flag corespunzator instructiunii
		 * 2. numar cate cifre corespunzatoare valorii parametrului
		 * 			sunt pana la sfarsitul instructiunii
		 * 3. transform in numar intreg valoarea
		 * 4. salvez in buffer-ul de iesire instructiunea si
		 * 			parametrul corespunzator
		 */

		if (buffer[1] == '_')
			switch (buffer[0])
			{
			case 'L':
				receivedCommand = TURN_LEFT;
				break;
			case 'R':
				receivedCommand = TURN_RIGHT;

				break;
			case 'S':
				receivedCommand = STOP_CHASSIS;
				break;
			case 'F':
				receivedCommand = MOVE_FORWARD;
				break;
			case 'B':
				receivedCommand = MOVE_BACKWARD;
				break;
			default:
				receivedCommand = UNKNOWN_INSTRUCTION;

			}
		else
			receivedCommand = INSTRUCTION_ERROR;

		// get parameter size
		while (buffer[parameterValuePower + 2] != ';')
			parameterValuePower++;

		for (uint8_t i = 0; i < parameterValuePower; i++)
		{
			// convert from ASCII chars to integer
			parameterValue *= 10;
			parameterValue += buffer[i + 2] - 48;
		}
	}
	outputInstructionsBuffer[0].instruction = receivedCommand;
	outputInstructionsBuffer[0].parameter = parameterValue;
	decodeInstructionsStatus = NEW_INSTRUCTIONS;
	read_ReceivedString_index += parameterValuePower + 3;
}

decodeRemoteInstructionsStatus_type remote_DecodeReceivedString(
		uint8_t *inputBuffer, instructionData_type *outputBuffer)
{
	if (decodeInstructionsStatus == INSTRUCTIONS_STORED)
		return INSTRUCTIONS_STORED;
	/*
	 iau dimensiunile tabloului
	 atata timp cat indexul nu a ajuns la sfarsit
	 transmit parametru functiiei de interpretare a comenzii adresa buffer-ului + indexul
	 dupa ce se citeste o instructiune la indexul global al inputBuffer-ului se adauga indexul local
	 */

	uint16_t inputBufferLength = 0;
	while (inputBuffer[inputBufferLength] != '\0')
		inputBufferLength++;

	// reset index for writing into instructions buffer
	if (write_InstructionBuffer_index == MAX_INSTRUCTIONS_NUMBER)
		write_InstructionBuffer_index = 0;

	// read content of received inputBuffer, decode and store into instructionBuffer
	while (read_ReceivedString_index < inputBufferLength)
	{
		remote_StoreInstruction(inputBuffer + read_ReceivedString_index,
				outputBuffer + write_InstructionBuffer_index);
		write_InstructionBuffer_index++;

	}
	return INSTRUCTIONS_STORED;
}

void remote_InstructionInterpretter(instructionData_type *instructionBuffer)
{

	static uint8_t read_InstructionBuffer_index = 0;

	instructionRunStatus_type instrRunStatus;

	/*
	 * read first instruction from buffer
	 * 		set busy flag
	 * 		set flag to execute instruction in main
	 */

	if (read_InstructionBuffer_index == MAX_INSTRUCTIONS_NUMBER)
	{
		read_InstructionBuffer_index = 0;
	}

	// do all the work here
	/*
	 * citire instructiunea corespunzatoare index-ului
	 * 	verificare cu switch case ce instructiune avem. sau nu, verificam daca instructiunea
	 * 		face parte din range-ul care e nevoie, si o trimitem ca parametru
	 *
	 */

	/*
	 * in fiecare caz se returneaza de la functie statusul executarii
	 * 		daca nu a fost indeplinita functia,
	 * 			statusul ei va fi BUSY, altfel DONE
	 */
	if (read_InstructionBuffer_index < write_InstructionBuffer_index)
	{
		uint16_t instructionParameter =
				instructionBuffer[read_InstructionBuffer_index].parameter;

		switch (instructionBuffer[read_InstructionBuffer_index].instruction)
		{
		case MOVE_FORWARD:
			instrRunStatus = move_Forward(instructionParameter);
			break;
		case MOVE_BACKWARD:
			// move back
			break;
		case TURN_RIGHT:
			instrRunStatus = rotate_Right(instructionParameter);
			break;
		case TURN_LEFT:
			instrRunStatus = rotate_Left(instructionParameter);
			break;
		case INSTRUCTION_ERROR:
			REMOTE.println("error");
			instrRunStatus = INSTRUCTION_DONE;
			resetFunc();
			break;
		case UNKNOWN_INSTRUCTION:
			REMOTE.println("unknown");
			instrRunStatus = INSTRUCTION_DONE;
			resetFunc();
			break;
		default:
			break;
		}
		if (instrRunStatus == INSTRUCTION_DONE)
			remoteInstructionRunStatus = REMOTE_INSTRUCTION_DONE;
		else
			remoteInstructionRunStatus = REMOTE_INSTRUCTION_BUSY;

		// go to next instruction
		if (remoteInstructionRunStatus == REMOTE_INSTRUCTION_DONE)
		{
			read_InstructionBuffer_index++;
		}
	}

}

void traceFollower_Main(void)
{
	static uint8_t recvBuffer[100];
	static instructionData_type instructionsBuffer[MAX_INSTRUCTIONS_NUMBER];

	decodeRemoteInstructionsStatus_type decodeStatus;

	if (REMOTE_RECEIVED_TRUE == remote_ReceiveInstructions(recvBuffer))
	{
		decodeStatus = remote_DecodeReceivedString(recvBuffer,
				instructionsBuffer);
	}
	if (decodeStatus == INSTRUCTIONS_STORED)
	{
		remote_InstructionInterpretter(instructionsBuffer);
	}

}
