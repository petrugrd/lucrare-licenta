/*
 * remote.h
 *
 *  Created on: 3 mai 2018
 *      Author: Petru Gurduza
 */

#ifndef REMOTE_SRC_REMOTE_H_
#define REMOTE_SRC_REMOTE_H_

#include <stdint.h>
#include <stdio.h>
#include "Welcome_Bot.h"

#define COMMAND_LENGTH 10
#define MAX_INSTRUCTIONS_NUMBER 20

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
#define REMOTE Serial
#else
#define REMOTE Serial3
#endif
typedef enum
{
	/*
	 * initially here are test instructions
	 * but there will be more instructions in the future development
	 * 		corresponding with platform purpose
	 *
	 *	|L -  left	|
	 *	|R - right	|
	 *	|S - stop	|
	 */
	COMMAND_ERROR, UNKNOWN_COMMAND, LEFT_MOTOR_POW, RIGHT_MOTOR_POW, STOP_MOTORS
} remoteInstructionReceivedOld_type;

typedef enum
{
	UNKNOWN_INSTRUCTION,
	MOVE_BACKWARD,
	MOVE_FORWARD,
	TURN_LEFT,
	TURN_RIGHT,
	STOP_CHASSIS,
	INSTRUCTION_ERROR
} remoteInstructionReceived_type;

typedef enum
{
	REMOTE_INSTRUCTION_BUSY, REMOTE_INSTRUCTION_DONE
} remoteInstructionRunStatus_type;

typedef enum
{
	NEW_INSTRUCTIONS, INSTRUCTIONS_STORED, DECODE_INSTRUCTION_ERROR
} decodeRemoteInstructionsStatus_type;

typedef enum
{
	REMOTE_RECEIVED_FALSE, REMOTE_RECEIVED_TRUE, REMOTE_IN_PROCESS
} remoteReceivedDataStatus_type;

typedef enum
{
	REMOTE_DECODE_BUSY, REMOTE_DECODE_DONE
} remoteDecodeStatus_type;
typedef struct
{
	remoteInstructionReceived_type instruction;
	uint16_t parameter;
} instructionData_type;

/*
 * 		Function declarations
 */

remoteReceivedDataStatus_type remote_ReceiveInstructions(uint8_t *receivedBuffer);
void remote_StoreInstruction(uint8_t * buffer,
		instructionData_type *outputInstructionsBuffer);
decodeRemoteInstructionsStatus_type remote_DecodeReceivedString(
		uint8_t *inputBuffer, instructionData_type *outputBuffer);
void remote_InstructionInterpretter(instructionData_type *instructionBuffer);

void traceFollower_Main(void);

#endif /* REMOTE_SRC_REMOTE_H_ */
