/*
 * trace_follower.h
 *
 *  Created on: 11 apr. 2018
 *      Author: Petru Gurduza
 */

#ifndef APPLICATION_TRACE_FOLLOWER_H_
#define APPLICATION_TRACE_FOLLOWER_H_
#include "Welcome_Bot.h"

typedef enum
{
	INSTRUCTION_BUSY, INSTRUCTION_DONE
} instructionRunStatus_type;


void TraceFollower_Init();
uint8_t drive_StraightForwards(uint16_t distance, uint8_t initialPower);
uint8_t drive_SetAngle(uint16_t angle);
uint8_t drive_RelativeAngle(uint8_t power, int8_t deltaAngle);


instructionRunStatus_type move_Forward(uint16_t distanceCm);
instructionRunStatus_type rotate_Left(uint16_t degree);
instructionRunStatus_type rotate_Right(uint16_t degree);

#endif /* APPLICATION_TRACE_FOLLOWER_H_ */

