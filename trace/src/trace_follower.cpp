/*
 * trace_follower.c
 *
 *  Created on: 11 apr. 2018
 *      Author: Petru Gurduza
 */
#include "trace_follower.h"

void TraceFollower_Init()
{
	Chassis_Init();
	HMC5883L_Init();
	incrmentalencoders_Init();
	REMOTE.begin(9600);
}

instructionRunStatus_type move_Forward(uint16_t distanceCm)
{
	/*	 daca distanta nu a fost atinsa
	 *		trimite putere motoarelor pentru a mentine unghiul
	 *
	 *	1. masoara unghiul curent
	 *	2. daca s-a mai executat vreodata calculeaza eroarea unghiului
	 *	3. seteaza putere motoarelor pentru a putea mentine unghiul
	 *	4.
	 */
	uint16_t currentDistance = inc_GetTotalDistance(INC_ENCODER_LEFT);
	static uint16_t oldDistance = currentDistance;
	static uint8_t check = 0;
	//uint8_t deltaAngle = 0;
	uint16_t deltaDistance = 0;

	uint8_t motorPower = 0xFF;

	if (check == 0)
	{
		oldDistance = currentDistance;

		check++;
		//
	}
	deltaDistance = currentDistance - oldDistance;

	if (deltaDistance <= distanceCm)
	{
		MotorLeftSetPower(motorPower);
		MotorRightSetPower(motorPower);
		return INSTRUCTION_BUSY;
	}
	else
	{
		check = 0;
		MotorLeft_STOP();
		MotorRight_STOP();
		return INSTRUCTION_DONE;
	}
}
int8_t motorPower = 100;

instructionRunStatus_type rotate_Left(uint16_t degree)
{
	static int16_t desiredAngle;
	static uint8_t check = 0;
	int16_t currentAngle = magnetometer_GetHeading(), deltaAngle;
	if (check == 0)
	{
		check++;
		desiredAngle = currentAngle + degree;
		if (desiredAngle < -180)
		{
			desiredAngle += 360;
		}
		else if (desiredAngle > 180)
		{
			desiredAngle += -360;
		}
	}

	deltaAngle = desiredAngle - currentAngle;
	if (deltaAngle > 180)
	{
		deltaAngle += -360;
	}
	else if (deltaAngle < -180)
	{
		deltaAngle += 360;
	}

#define DEBUGGING
#ifdef DEBUGGING
	Serial.println();
	Serial.print("Curr ang: ");
	Serial.print(currentAngle);
	Serial.print("\t\t");
	Serial.print("Dt ang: ");
	Serial.print(deltaAngle);
	Serial.print("\t\t");
	Serial.print("Des an: ");
	Serial.println(desiredAngle);
#endif

	if (deltaAngle > 3 || deltaAngle < -3)
	{
		MotorRightSetPower(motorPower);
		MotorLeftSetPower(0);
		return INSTRUCTION_BUSY;
	}
	else
	{
		check = 0;
		MotorLeft_STOP();
		MotorRight_STOP();

		return INSTRUCTION_DONE;
	}
}

instructionRunStatus_type rotate_Right(uint16_t degree)
{
	static int16_t desiredAngle;
	static uint8_t check = 0;
	int16_t currentAngle = magnetometer_GetHeading(), deltaAngle;
	if (check == 0)
	{
		check++;
		desiredAngle = currentAngle - degree;
		if (desiredAngle > 180)
		{
			desiredAngle += -360;
		}
		else if (desiredAngle < -180)
		{
			desiredAngle += 360;
		}
	}
	deltaAngle = desiredAngle - currentAngle;

	if (deltaAngle < -180)
	{
		deltaAngle += 360;
	}
	else if (deltaAngle > 180)
	{
		deltaAngle += -360;
	}


	if (abs(deltaAngle) > 6)
	{
		MotorLeftSetPower(motorPower);
		MotorRightSetPower(0);
		return INSTRUCTION_BUSY;
	}
	else
	{
		check = 0;
		MotorLeft_STOP();
		MotorRight_STOP();
		return INSTRUCTION_DONE;
	}
}
